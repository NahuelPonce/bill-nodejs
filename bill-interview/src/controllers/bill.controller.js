const db = require("../models");
const Bill = db.bills;
const Op = db.Sequelize.Op;

//create and save a new Bill

exports.create = (req, res) => {
    //Validate request
    if (!req.body.reference) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

//Create a Bill
console.log(req.body.reference);
const bill = {
    reference: req.body.reference,
    total: req.body.total,
    observation: req.body.observation
};
//Save Bill in the db
Bill.create(bill)
    .then(data => {
        res.status(200).send(data);
    })
    .catch(err => {
        res.status(400).send({
            message:
            err.message || "Some error occurred while creating the Bill"
        });
    });
};

//Update a Bill by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
    Bill.update(req.body, {
        where: { id: id }
    })
    .then(num => {
        if (num == 1) {
            res.status(201).send({
                message: "Bill was update successfully"
            });
        } else {
            res.status(404).send({
                message: `Cannot update Bill with id=${id}. Maybe Bill was not found.`
            });
        }
    })
    .catch(err => {
        res.status(400).send({
            message: "Error updating Bill with id=" + id
        });
    });
};

// Delete a Bill with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
    Bill.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.status(201).send({
            message: "Bill was deleted successfully!"
          });
        } else {
          res.status(404).send({
            message: `Cannot delete Bill with id=${id}. Maybe Bill was not found!`
          });
        }
      })
      .catch(err => {
        res.status(400).send({
          message: "Could not delete Bill with id=" + id
        });
      });
  };