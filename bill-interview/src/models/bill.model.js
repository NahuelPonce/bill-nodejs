module.exports = (sequelize, Sequelize) => {

    const Bill = sequelize.define("bill", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        reference: {
            type: Sequelize.STRING,
            allowNull: false
        },
        
        total: {
            type: Sequelize.DECIMAL(10,2),
            allowNull: false
        },
        observation: {
            type: Sequelize.STRING,
            allowNull: true
        }
    });

    return Bill;
};