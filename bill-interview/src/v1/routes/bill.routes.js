module.exports = app => {
    const bills = require("../../controllers/bill.controller");

    var router = require("express").Router();
/** 
* @openapi
* components:
*   schemas:
*       Bill:
*           type: object
*           properties:
*               reference:
*                   type: string
*               total:
*                   type: number
*               observation:
*                   type: string
*           required:
*              - reference
*              - total
*/
/**
 * @openapi
 * /admin/bill:
 *   post:
 *     summary: Create a new Bill
 *     tags:
 *       - Bills
 *     requestBody:
 *      content:
 *       application/json:
 *        schema:
 *          type: object
 *          items:
 *            $ref: '#/components/schemas/Bill'
 *          example:
 *            reference: Industrial
 *            total: 26000.56
 *            observation: Maquinaria para produccion de bebidas
 *     responses:
 *       200:
 *         description: Bill was created succesfelly
 *       400:
 *         description: Some error occurred while creating the Bill

 */
    //Create a new Bill
    router.post("/", bills.create);

/**
 * @openapi
 * /admin/bill/{id}:
 *   put:
 *     summary: Update a Bill
 *     tags:
 *       - Bills
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *              type: integer
 *              required: true
 *              description: the bill id
 *     requestBody:
 *      content:
 *       application/json:
 *        schema:
 *          type: object
 *          items:
 *            $ref: '#/components/schemas/Bill'
 *          example:
 *            total: 56000.56
 *     responses:
 *       200:
 *         description: Bill was updated succesfelly
 *       404:
 *         description: Bill was not found
 *       400:
 *          description: Error updating Bill
 */
    //Update a Bill with id
    router.put("/:id", bills.update);

/**
 * @openapi
 * /admin/bill/{id}:
 *   delete:
 *     summary: Delete a Bill
 *     tags:
 *       - Bills
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *              type: integer
 *              required: true
 *              description: the bill id
 *     responses:
 *       200:
 *         description: Bill was deleted successfully
 *       404:
 *         description: Bill was not found
 *       400:
 *          description: Error deleting Bill
 */
    //Delete a Bill with id
    router.delete("/:id", bills.delete);

    app.use('/admin/bill', router);
};