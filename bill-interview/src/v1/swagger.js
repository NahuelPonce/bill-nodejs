const swaggerJSDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

//Metadata info about our API
const options = {
    definition: {
        openapi: "3.0.0",
        info: { title: 'Bill API', version: '1.0.0'},
    },
    apis: ['src/v1/routes/bill.routes.js']
};

//Docs en JSON format
const swaggerSpec = swaggerJSDoc(options);

//Function to setup our docs
const swaggerDocs = (app, port) => {
    app.use('/admin/bill/v1/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
    app.get('/admin/bill/v1/docs.json', (req, res) => {
        res.setHeader("Content-Type", "application/json");
        res.send(swaggerSpec);
    });

    console.log(`Version 1 Docs are available at http://localhost:${port}/admin/bill/v1/docs`)
};

module.exports = { swaggerDocs };